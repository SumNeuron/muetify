import marked from 'marked'

const headerClassLookup = {
  1: 'display-4',
  2: 'display-3',
  3: 'display-2',
  4: 'display-1',
  5: 'headline',
  6: 'title'
}


const heading = (text, level) => {
  return `<h${level} class="muetify ${headerClassLookup[level]}">${text}</h${level}>`
}

const paragraph = (text) => {
  return `<p class="muetify  body-2">${text}</p>`
}

const code = (text, info, isEscaped) => {
  return `<pre><code class="muetify">${text}</code></pre>`
}

const listitem = (text, isTask, isChecked) => {
  return `<li class="muetify body-1">${text}</li>`
}


const blockquote = (text) => {
  return `<blockquote><p class="muetify subheading">${text}</p></blockquote>`
}


const renderer = new marked.Renderer()
// Override function
renderer.heading = heading
renderer.paragraph = paragraph
renderer.code = code
renderer.listitem = listitem
renderer.blockquote = blockquote


export default renderer

import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify-es';
import minimist from 'minimist';

import pkg from './package.json';

const argv = minimist(process.argv.slice(2));

const config = {
  input: 'src/index.js',
  external: [
    'marked',
  ],
  output: {
    extend: true,
    name: pkg.name,
    exports: 'named',
    globals: {
      'marked': 'marked',
    },
  },
  plugins: [
    babel({
      exclude: 'node_modules/**',
      // runtimeHelpers: true,
      externalHelpers: true,
      plugins: [
        [
          'wildcard',
          {
            exts: [],
            nostrip: true,
          },
        ],
        '@babel/plugin-external-helpers',
      ],
      presets: [
        ['@babel/preset-env', {modules: false,},],
      ],
    }),
  ],
};

// Only minify browser (iife) version
if (argv.format === 'iife') {
  config.plugins.push(uglify());
}

export default config;
